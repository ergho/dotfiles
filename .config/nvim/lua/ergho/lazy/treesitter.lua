return {
	"nvim-treesitter/nvim-treesitter",
	parser_install_dir = "/home/ergho/.local/share/nvim/nvim-treesitter/parsers",
	build = ":TSUpdate",
	config = function()
		require("nvim-treesitter.configs").setup({
			ensure_installed = {
				"vimdoc",
				"javascript",
				"c",
				"lua",
				"python",
				"go",
				"rust",
				"bash",
				"yaml",
			},

			sync_install = false,

			auto_install = true,

			indent = {
				enable = true,
			},

			highlight = {
				enable = true,

				additional_vim_regex_highlighting = { "markdown" },
			},
		})
	end,
}
