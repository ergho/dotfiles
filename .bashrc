# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

stty stop ''
stty start ''
stty -ixon
stty -ixoff
export XDG_CONFIG_HOME=~/.config
export EDITOR=nvim
export PATH=$PATH:$HOME/.local/bin:/usr/local/go/bin:$HOME/wowsaker
export TERM=xterm-256color
export SOPS_AGE_KEY_FILE=~/.config/sops/age/ergho.agekey
PS1='[\u@\h \W]\$'
source ~/.bashaliases
source ~/.bashfunctions
