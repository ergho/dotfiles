
set guicursor=
set nohlsearch
set noerrorbells
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set relativenumber number
set nowrap
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set scrolloff=10
set signcolumn=yes
set termguicolors
set noshowmode
set completeopt=menuone,noinsert,noselect

" Having a longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience
set updatetime=50

" Make more space for displaying messages
set cmdheight=2

" Dont pass messages to |ins-completion-menu|
set shortmess+=c
" Changes direction of splitting
"set splitbelow
"set splitright
"
" adds column to maintain code width of 80
set colorcolumn=80

"for lightline
set laststatus=2
